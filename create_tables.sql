create database syshogs;

create table processes
(
	uid INT(7) NOT NULL,
	pid INT(7) NOT NULL,
	pname VARCHAR(250) NOT NULL,
	vmsize BIGINT(15) NOT NULL,
	jiffies BIGINT(15) NOT NULL,
	added_on DATETIME NOT NULL
);

create table dependencies
(
	uid INT(7) NOT NULL,
	pid INT(7) NOT NULL,
	pathname VARCHAR(250) NOT NULL,
	added_on DATETIME NOT NULL
);
